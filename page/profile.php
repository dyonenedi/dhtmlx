<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="dhtmlx/skins/skyblue/dhtmlx.css"/>
    <script src="dhtmlx/codebase/dhtmlx.js"></script>

    <script>
			var grid;
			var tree;
			var userId = 0;

			function doOnLoadContent() {
				// GRID 1
				grid = new dhtmlXGridObject('grid');
				grid.setImagePath("dhtmlx/sources/dhtmlxgrid/codebase/imgs/dhxgrid_web/");
				grid.setHeader("Name, Email, Ativo");
				grid.attachHeader("#combo_filter,#combo_filter,#combo_filter");
				grid.setInitWidths("*,150,100");
			  //grid.enableAutoWidth(true);
				grid.setColAlign("left,left,center");
				grid.setColTypes("ed,ed,ed");
				grid.setColSorting("str,str,str");
				//grid.setSkin("dhx_skyblue");
				grid.init();
				grid.enableSmartRendering(true);
				grid.loadXML("ajax/get_users.php");
				grid.attachEvent('onRowSelect', function(id, ind){
					loadGrid2(id);
				});

				// GRID 2
        tree = new dhtmlXTreeObject("tree","100%","100%",0);
        tree.setImagePath("dhtmlx/sources/dhtmlxTree/codebase/imgs/dhxtree_terrace/");
        tree.enableSmartXMLParsing(true);

			}

			function loadGrid2(id){
        userId = id;
        tree.deleteChildItems(0)
				tree.loadJSON("ajax/get_profile.php?userId="+userId);
			}
		</script>
  </head>

  <body onLoad="doOnLoadContent();" style="margin:0px;padding:0px;font-family:'Roboto',arial,sans-serif; color: #999;">
    <div id="grid" style="margin:0px;padding:0px;width:70%;height:600px;background-color:white; display:inline-block;"></div>
    <div id="tree" style="margin:0px;padding:0px;width:29%;height:600px; display:inline-block; background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;""></div>
  </body>
</html>
