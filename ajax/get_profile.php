<?php
	require_once('../class/classes.php');
	$user = New User();

	function getData($data, $reply){
		$user = New User();

		foreach($data AS $key => $profile){
				$reply['item'][$key] = [
					'id'=>$profile['FILHO'], 'text'=>$profile['TELA']
				];

				if ($user->isProfileParent($_GET['userId'], $profile['FILHO'])) {
					$data = $user->getProfileById($_GET['userId'], $profile['FILHO']);
					$reply['item'][$key] = getData($data, $reply['item'][$key]);
				}
		}

		return $reply;
	}


	$data = $user->getProfileById($_GET['userId'], '9999');

	$reply = ['id'=>0];
	$reply = getData($data, $reply);


	header('Content-Type: aplication/json; charset=utf-8');
	echo json_encode($reply);
