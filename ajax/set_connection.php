<?php
  	session_start();
    $exp        = explode('_', $_GET['conn']);
    $location   = $exp[0];
    $connection = $exp[1];

    if ($connection == 'production') {
      $_SESSION['CONNECTION'] = $connection;
      $_SESSION['LOCATION'] = $location;
    } elseif ($connection == 'statement') {
      $_SESSION['CONNECTION'] = $connection;
      $_SESSION['LOCATION'] = $location;
    } else {
      $_SESSION['CONNECTION'] = 'test';
      $_SESSION['LOCATION'] = 'todos';
    }

    echo true;
