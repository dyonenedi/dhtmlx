<?php
	$menu = [
		'id' => 0, 'item' => [
			[
				'id' => 1,
				'text' => 'Usuário',
				'open' => 1,
				'item' => [
					[
						'id'=> 'profile',
						'text' =>	'Perfil'
					]
				],
			],
			[
				'id' => 2,
				'text' => 'Manufatura',
				'open' => 1,
				'item' => [
					[
						'id'=> 'Gestão da Produção',
						'text' =>	'gp'
					]
				]
			],
		],
	];

	header('Content-Type: aplication/json; charset=utf-8');
	echo json_encode($menu);
