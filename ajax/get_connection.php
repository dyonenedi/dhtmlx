<?php
	$connection = [
		'id' => 0, 'item' => [
			[
				'id' => 1,
				'text' => 'Todos',
				'item' => [
					['id'=> 'todos_test', 'text' => 'Teste']
				],
			],
			[
				'id' => 2,
				'text' => 'Curitiba',
				'item' => [
					['id'=> 'curitiba_statement', 'text' => 'Homologação'],
					['id'=> 'curitiba_production', 'text' => 'Produção'],
				],
			],
			[
				'id' => 3,
				'text' => 'Sorocaba',
				'item' => [
					['id'=> 'sorocaba_production', 'text' => 'Produção'],
				],
			],
			[
				'id' => 4,
				'text' => 'Piracicaba',
				'item' => [
					['id'=> 'piracicaba_production', 'text' => 'Produção'],
				],
			],
			[
				'id' => 5,
				'text' => 'Iveco',
				'item' => [
					['id'=> 'iveco_production', 'text' => 'Produção'],
				],
			],
		],
	];

	header('Content-Type: aplication/json; charset=utf-8');
	echo json_encode($connection);
