<?php
	class ConexaoOracle{

		private static $charset = 'AL32UTF8';
	  private static $instance;
		private static $host;
		private static $port;
		private static $serviceName;
		private static $user;
		private static $pass;
		private static $db;

		public static $scheme;


	  public static function getInstance(){
			if ($_SESSION['CONNECTION'] == 'production') {
				if ($_SESSION['LOCATION'] == 'curitiba') {
					//Produção CURITIBA
					self::$scheme = 'CURITIBA_PROD';
					self::$host = '146.47.248.13';
					self::$port = '1521';
					self::$serviceName = 'MMSCUR';
					self::$user = 'READONLY';
					self::$pass = 'READONLY';
				} else if ($_SESSION['LOCATION'] == 'sorocaba') {
					//Produção SOROCABA
					self::$scheme = 'SOROCABA_PROD';
					self::$host = '146.47.14.228';
					self::$port = '1521';
					self::$serviceName = 'mmssor';
					self::$user = 'READONLY';
					self::$pass = 'READONLY';
				} else if ($_SESSION['LOCATION'] == 'piracicaba') {
					//Produção PIRACICABA
					self::$scheme = 'PIRACICABA_PROD';
					self::$host = '146.47.14.228';
					self::$port = '1521';
					self::$serviceName = 'TAF_MMSPIR';
					self::$user = 'READONLY';
					self::$pass = 'READONLY';
				} else if ($_SESSION['LOCATION'] == 'iveco') {
					//Produção IVECO
					self::$scheme = 'IVECO';
					self::$host = '172.20.46.20';
					self::$port = '1521';
					self::$serviceName = 'IVECO7L';
					self::$user = 'READONLY';
					self::$pass = 'READONLY';
				}
			} else if ($_SESSION['CONNECTION'] == 'statement') {
				if ($_SESSION['LOCATION'] == 'curitiba') {
					//HOMOLOGAÇÃO CURITIBA
					self::$scheme = 'CURITIBA_HOM';
					self::$host = 'cbrcur01apcp100';
					self::$port = '1522';
					self::$serviceName = 'MMSCUR';
					self::$user = 'READONLY';
					self::$pass = 'READONLY';
				}
			} else if ($_SESSION['CONNECTION'] == 'test') {
				if ($_SESSION['LOCATION'] == 'todos') {
					//TESTE
					self::$scheme = '';
					self::$host = 'cbrcur01apcp100';
					self::$port = '1527';
					self::$serviceName = 'DEV';
					self::$user = 'TESTE';
					self::$pass = 'Teste#123';
				}
			}

			if(self::$instance === null){
				self::$db = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = ".self::$host.")(PORT = ".self::$port.")))(CONNECT_DATA=(SERVICE_NAME=".self::$serviceName.")))";
				self::$instance = oci_connect(self::$user, self::$pass, self::$db, self::$charset);

				if (!self::$instance) {
					 $e = oci_error();
					exit($e['message']);
				}
      }

      return self::$instance;
		}
	}

	Class Db
	{
		private $_conn;
		private function connect(){
			$this->_conn = ConexaoOracle::getInstance();
			$this->modifyScheme();
		}

		private function modifyScheme(){
			if (ConexaoOracle::$scheme) {
				$sql = "ALTER session SET current_schema = ".ConexaoOracle::$scheme;
				$query = oci_parse($this->_conn, $sql);
				oci_execute($query);
			}
		}

		public function findEqual($table, $where, $limit='all'){
			$this->connect();

			$data = [];
			foreach ($where AS $key => $value) {
				$x[] = "LOWER(".$key.") = '". strtolower($value) ."'";
			}

			$where = implode(' AND ', $x);

			$sql = "SELECT * FROM ".$table." WHERE ".$where;
			$query = oci_parse($this->_conn, $sql);
			oci_execute($query);
			oci_fetch_all($query, $data, null, null, OCI_FETCHSTATEMENT_BY_ROW);

			$this->desconnect();

			$limit = ($limit == 'one') ? 1 : 0;
			return ($limit == 1) ? $data[0] : $data;
		}

		public function findLike($table, $where, $limit='all'){
			$this->connect();

			$data = [];
			foreach ($where AS $key => $value) {
				$x[] = "LOWER(".$key.")" . " LIKE '%". strtolower($value) ."%'";
			}

			$where = implode(' AND ', $x);

			$sql = "SELECT * FROM ".$table." WHERE ".$where;
			$query = oci_parse($this->_conn, $sql);
			oci_execute($query);
			oci_fetch_all($query, $data, null, null, OCI_FETCHSTATEMENT_BY_ROW);

			$this->desconnect();

			$limit = ($limit == 'one') ? 1 : 0;
			return ($limit == 1) ? $data[0] : $data;
		}

		public function findSql($sql){
			$this->connect();
			$query = oci_parse($this->_conn, $sql);
			oci_execute($query);
			oci_fetch_all($query, $data, null, null, OCI_FETCHSTATEMENT_BY_ROW);

			$this->desconnect();
			return $data;
		}

		public function findRow($sql){
			$this->connect();
			$query = oci_parse($this->_conn, $sql);
			oci_execute($query);
			$data = oci_fetch($query);
			$this->desconnect();
			return $data;
		}

		private function desconnect(){
			$this->_conn = null;
		}
	}
