<?php
	session_start();
	
	require_once('db.php');

	Class User extends Db
	{
		public $data;

		public function getUserByName($name){
			$this->data = $this->findLike('TBLZ03', ['Z03_NOME' => $name], 'all');
		}

		public function getUserById($id){
			$this->data = $this->findEqual('TBLZ03', ['Z03_ID' => $id], 'one');
		}

		public function getProfileById($userId, $childId){
			$sql = "
				SELECT
				  Z05_FILHO AS FILHO,
				  Z06_DESC AS TELA
				FROM
				  TBLZ05
				INNER JOIN TBLZ06
				ON TBLZ05.Z05_FILHO = TBLZ06.Z06_COD
				INNER JOIN TBLZ03
				ON TBLZ05.Z05_USER = TBLZ03.Z03_LOGIN
				WHERE Z05_PAI = '".$childId."' AND Z03_ID = '".$userId."'
				ORDER BY Z05_ORDEM
			";

			$data = $this->findSql($sql);
			return $data;
		}

		public function isProfileParent($userId, $childId){
			$sql = "
				SELECT
					Z05_ID
				FROM
					TBLZ05
					INNER JOIN
					  TBLZ03
					ON
						TBLZ05.Z05_USER = TBLZ03.Z03_LOGIN
				WHERE Z05_PAI = '".$childId."' AND Z03_ID = '".$userId."'
			";

			$result = $this->findRow($sql);

			return $result;
		}
	}

	Class Me extends Db
	{
		public $id;
		public $login;
		public $nome;
		public $sala;
		public $senha;
		public $ativo;
		public $nivel;
		public $email;

		public function __construct($name){
			$userData = $this->findLike('TBLZ03', ['Z03_NOME' => $name], 'one');

			$this->id = $userData['Z03_ID'];
			$this->login = $userData['Z03_LOGIN'];
			$this->nome = $userData['Z03_NOME'];
			$this->sala = $userData['Z03_SALA'];
			$this->senha = $userData['Z03_SENHA'];
			$this->ativo = $userData['Z03_ATIVO'];
			$this->nivel = $userData['Z03_NIVEL'];
			$this->email = $userData['Z03_EMAIL'];
		}
	}
