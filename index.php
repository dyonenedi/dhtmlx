<?php
	session_start();
	$_SESSION['CONNECTION'] = (!empty($_SESSION['CONNECTION'])) ? $_SESSION['CONNECTION'] : 'test';
	$_SESSION['LOCATION'] = (!empty($_SESSION['LOCATION'])) ? $_SESSION['LOCATION'] : 'curitiba';
?>

<!DOCTYPE html>
<html>
	<head>
		<title>User</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

		<link rel="stylesheet" type="text/css" href="skin/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="dhtmlx/codebase/dhtmlx.css"/>
		<link rel="stylesheet" type="text/css" href="dhtmlx/skins/web/dhtmlx.css"/>

		<script src="skin/jquery.min.js"></script>
		<script src="skin/bootstrap.min.js"></script>
		<script src="dhtmlx/codebase/dhtmlx.js"></script>
		<style>
			div#contentObj {
				position: relative;
  			width: 100%;
				margin: 0;
				padding: 0;
			  border: 0;
			  outline: 0;
			}
			div#layoutObj {
				box-shadow: none;
			  width: 1200px;
				height: 625px;
				background-color: #fff;
				box-shadow: 0 2px 5px 0 rgba(0,0,0,.26);
				box-sizing: border-box;
				position: relative;
				display: inline-block;
				margin: 0;
				left: 50%;
			  margin-left: -600px; /* A metade de sua largura. */
			  position: absolute;
			}
		</style>
		<script>
			var layout;
			var tree;
			var tabbar;
			var tabbarIndex = 1;
			var tabbarStore = new Array();

			function doOnLoad() {
				if (layout != null) {
					layout.unload();
					tree = layout = null;
				}

				// INIT LAYOUT
				layout = new dhtmlXLayoutObject({
					parent: "layoutObj",
					pattern: "3W",
					cells: [
						{id: "a", header: false, width: 200},
						{id: "b", header: false},
						{id: "c", text: 'Conexões', width: 150, dock: 0}
					]
				});

				// INIT MENU
				tree = layout.cells("a").attachTree();
				tree.setImagePath("dhtmlx/sources/dhtmlxTree/codebase/imgs/dhxtree_skyblue/");
				tree.setOnLoadingStart(function(){layout.cells("a").progressOn();});
				tree.setOnLoadingEnd(function(){layout.cells("a").progressOff();});
				tree.setOnClickHandler(openIframe);
				tree.enableSmartXMLParsing(true);
				tree.loadJSON("ajax/get_menu.php");

				// INIT CONTENT WITH TABBAR
				tabbar = layout.cells("b").attachTabbar();
				tabbar.enableTabCloseButton(true);
				tabbar.attachEvent("onTabClose", function(id){
				 	delete tabbarStore[id];
					localStorage['tabbar'] = JSON.stringify(tabbarStore);
					return true;
				});

				// INIT DB
				tree2 = layout.cells("c").attachTree();
				tree2.setImagePath("dhtmlx/sources/dhtmlxTree/codebase/imgs/dhxtree_terrace/");
				tree2.setOnLoadingStart(function(){layout.cells("c").progressOn();});
				tree2.setOnLoadingEnd(function(){layout.cells("c").progressOff();});
				tree2.enableSmartXMLParsing(true);
				tree2.setOnClickHandler(setConnection);
				tree2.loadJSON("ajax/get_connection.php");
				layout.cells("c").collapse();

				// LOAD OPENED TABBAR
				if (localStorage.length > 0 && localStorage['tabbar'].length > 0) {
					tabbarStore = JSON.parse(localStorage['tabbar']);
					localStorage['tabbar'] = new Array();

					var i = 1;
					var tabbarStoreAux = new Array();
					tabbarStore.forEach(function(page, ind){
							if (page != null) {
								tabbarStoreAux[i] = page;
								i++;
							}
					});
					tabbarStore = tabbarStoreAux;

					if (tabbarStore.length > 0) {
						tabbarStore.forEach(function(page, ind){
								openIframe(page);
						});
					} else {
						openIframe('home');
					}
				} else {
						openIframe('home');
				}
			}

			function openIframe(page){
				if (!parseInt(page)) {
					tabbar.addTab(tabbarIndex, page, null, null, true, true);
					tabbar.tabs(tabbarIndex).attachURL("router.php?page="+page);

					tabbarStore[tabbarIndex] = page;
					localStorage['tabbar'] = JSON.stringify(tabbarStore);

					tabbarIndex++;
				}
			}

			function setConnection(id){
				if (!parseInt(id)) {
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.open("GET", "ajax/set_connection.php?conn="+id, true);
					xmlhttp.send();
					setTimeout(function(){location.reload();}, 500);
				}
			}
		</script>
	</head>

	<body onload="doOnLoad();" style="background: #f5f5f5;">
		<nav class="navbar navbar-inverse" style="border-radius: 0;">
      <div class="container content">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
          </button>
          <a class="navbar-brand" href="/dev/">MMS - <?php echo ucfirst($_SESSION['CONNECTION'])." / ".ucfirst($_SESSION['LOCATION']); ?></a>
        </div>
      </div><!-- /.container-fluid -->
    </nav>
		<div id="contentObj">
			<div id="layoutObj"></div>
		</div>
	</body>
</html>
