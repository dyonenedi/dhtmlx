<?php
  session_start();
  
  $page = (!empty($_GET['page'])) ? $_GET['page']: 'home';
  $path = __DIR__."/page/".$page.".php";
  if (file_exists($path)) {
      require_once("page/".$page.".php");
  } else {
      require_once("page/error.php");
  }
